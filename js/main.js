$(document).ready(function(){

	// aos
	AOS.init();
	// aos end

	// wow
	new WOW().init();
	// wow end

	// banner
	var owl = $('.main-banner__owl');
	owl.owlCarousel({
		items:1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 15000,
		smartSpeed:800
	});
	// banner end

	$('.card-carousel').owlCarousel({
		dots: true,
		dotsContainer: '#custom-dots',
		items:1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 2000,
		smartSpeed:800,
		margin: 3
	});
	$('.owl-dot').click(function () {
		owl.trigger('to.owl.carousel', [$(this).index(), 300]);
	});

	// superfish
	$('.open-menu').superfish({
		delay: 200,
		animation: {opacity:'show',height:'show'},
		animationOut: {opacity:'hide', height: 'hide'}
	});

	$('.phone').mask("8(999)-999-99-99");

	// search
	$('.search').click(function(){

		$(this).addClass('active-search')
		$('.header-contact-callback').css("display","none");
		$('.header-wrapper-left-link ul').addClass('active');
		$('.header-wrapper-left-contact').css({"margin-top":"2rem","margin-left":"1rem"});
		$('.header-contact-search form button img').addClass('active-img');
		$('.header-contact-search button').css("display","block");
	});
	$('.btn-close').click(function(e){
		e.preventDefault();
		$('.search').removeClass('active-search');
		$('.header-contact-callback').css("display","flex");
		$('.header-wrapper-left-link ul').removeClass('active');
		$('.header-wrapper-left-contact').css({"margin-top":"0","margin-left":"0"});
		$('.header-contact-search form button img').removeClass('active-img');
		$('.btn-close').css("display","none");
	});

	$('.contact-info ul li a').click(function(e){
		e.preventDefault();
	});

	$('.left-bar a').click(function(e){
		e.preventDefault();
		var attr = $(this).attr('href');
		if($(attr).css('display') == 'none'){
			$(attr).animate({height: 'show'}, 500); 
		}else{
			$(attr).animate({height: 'hide'}, 500); 
		}

	});


	$(".hamburger").on("click", function() {
		$(this).parent(".hamburger-wrapper").toggleClass("hamburger-active")
	});

	$('.cabinet-tab a').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');

		$('.cabinet-tab a').removeClass('cabinet-active-tab');
		$(this).addClass('cabinet-active-tab');

		$('.wrapper-info').removeClass('wrapper-cabinet-active');
		$(attr).addClass('wrapper-cabinet-active');
	});

	$('.open-menu-mobile a').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');
		if($(attr).css('display') == 'none'){
			$(attr).animate({height: 'show'}, 500); 
		}else{
			$(attr).animate({height: 'hide'}, 500); 
		}
	});

	$('.sea-password-btn').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');
		let namebtn = $(this).html();
		if (namebtn == '<i class="fas fa-eye-slash"></i> Скрыть пароль') {
			var type = $(attr).attr('type','password');
			$(this).html('<i class="fas fa-eye"> Показать пароль');
		}else{
			var type = $(attr).attr('type','text');
			$(this).html('<i class="fas fa-eye-slash"></i> Скрыть пароль');
		}

	});
	$('.addAdress').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');

		$(attr).append('<input type="text" class="form-control" name="" placeholder="Ваш адрес">');
	});
	;
	// partners carousel

	$('.partners-carousel').owlCarousel({
		items:6,
		loop: true,
		autoplay: true,
		autoplayTimeout: 1500,
		smartSpeed:800,
		margin: 3,
		responsive : {
				0 : {
					items: 1
				},

				600 : {
					items: 3
				},

				1024 : {
					items: 6
				}
			}
	});


});

